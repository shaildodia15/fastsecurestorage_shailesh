//
//  ViewController.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shailesh Dodia on 17/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //*Path to SQLite file
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        print(paths[0],"\n")
 
        FastSecureStorage.set(string: "Shailesh", forKey: "fname")
        print("String: ",FastSecureStorage.string(forKey: "fname") ?? "","\n")

        FastSecureStorage.set(decimal: 10, forKey: "decimal")
        print("Decimal: ",FastSecureStorage.decimal(forKey: "decimal") ?? "","\n")

        FastSecureStorage.set(int: -300, forKey: "int")
        print("Int: ",FastSecureStorage.int(forKey: "int") ?? "","\n")

        FastSecureStorage.set(float: 30.00, forKey: "float")
        print("Float: ",FastSecureStorage.float(forKey: "float") ?? "","\n")

        FastSecureStorage.set(double: 40.00, forKey: "double")
        print("Double: ",FastSecureStorage.double(forKey: "double") ?? "","\n")

        FastSecureStorage.set(boolean: true, forKey: "boolean")
        print("Boolean: ",FastSecureStorage.boolean(forKey: "boolean") ?? "","\n")


        let arrayColor: [String] = ["red", "green", "blue"]
        FastSecureStorage.set(array: arrayColor, forKey: "arrayColor")
        print("Array: ",FastSecureStorage.array(forKey: "arrayColor") ?? "","\n")

        let dictionary: [String: String] = ["name":"shailesh", "id":"1212", "city":"surat"]
        FastSecureStorage.set(dictionary: dictionary, forKey: "dictionary")
        print("Dictionary: ",FastSecureStorage.dictionary(forKey: "dictionary") ?? "","\n")


        let updateProfile = UpdateProfile(name: "Shailesh Dodia", age: 28)
        FastSecureStorage.set(customObject: updateProfile, inKey: "CustomObject")

        let obj = FastSecureStorage.customObject(object: UpdateProfile.self, fromKey: "CustomObject")
        if let name = obj?.name, let age = obj?.age
        {
            print("Custom Class: ",name, terminator: ": ")
            print(age)
        }

    }
}


class UpdateProfile: Codable
{
    let name: String
    let age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}




