//
//  CryptoFUtility.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shailesh Dodia on 19/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation
import CommonCrypto

class CryptoUtility: NSObject
{
    static func Crypt(data:Data, operation:Int) -> Data?
    {
        let keyData  = "c292a6e6c19b7403cd87949d0ad41221".data(using:String.Encoding.utf8)!  //Length of secret key should be 32 for 256 bits key size
        let ivData  = "288dca8158b1dd7c".data(using:String.Encoding.utf8)!  //Length of initialization vector must be 16 with AES.
        
        let cryptLength  = size_t(data.count + kCCKeySizeAES256)
        var cryptData = Data(count:cryptLength)
        
        let keyLength  = size_t(kCCKeySizeAES256)
        let options   = CCOptions(kCCOptionPKCS7Padding)
        
        
        var numBytesEncrypted :size_t = 0
        
        let cryptStatus = cryptData.withUnsafeMutableBytes {cryptBytes in
            data.withUnsafeBytes {dataBytes in
                ivData.withUnsafeBytes {ivBytes in
                    keyData.withUnsafeBytes {keyBytes in
                        CCCrypt(CCOperation(operation),
                                CCAlgorithm(kCCAlgorithmAES128),
                                options,
                                keyBytes, keyLength,
                                ivBytes,
                                dataBytes, data.count,
                                cryptBytes, cryptLength,
                                &numBytesEncrypted)
                    }
                }
            }
        }
        
        if UInt32(cryptStatus) == UInt32(kCCSuccess)
        {
            cryptData.removeSubrange(numBytesEncrypted..<cryptData.count)
        }
        else
        {
            print("Error: \(cryptStatus)")
        }
        return cryptData
    }
}

