//
//  FastSecureStorage.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shailesh Dodia on 17/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import UIKit

class FastSecureStorage: NSObject
{
    //Storing String//
    static func set(string: String, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: string, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving String//
    static func string(forKey: String) -> String?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnString =  returnData as? String
            return returnString
        }
        return nil
    }
    
    
    //Storing Decimal//
    static func set(decimal: Decimal, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: decimal, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving Decimal//
    static func decimal(forKey: String) -> Int?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnDecimal =  returnData as? Int
            return returnDecimal
        }
        return nil
    }
    
    
    //Storing Int//
    static func set(int: Int, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: int, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving Int//
    static func int(forKey: String) -> Int?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnInt =  returnData as? Int
            return returnInt
        }
        return nil
    }
    
    
    //Storing Float//
    static func set(float: Float, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: float, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving Float//
    static func float(forKey: String) -> Float?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnFloat =  returnData as? Float
            return returnFloat
        }
        return nil
    }
    
    
    //Storing Double//
    static func set(double: Double, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: double, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving Double//
    static func double(forKey: String) -> Double?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnDouble =  returnData as? Double
            return returnDouble
        }
        return nil
    }
    
    
    //Storing Boolean//
    static func set(boolean: Bool, forKey:String)
    {
        DataModelStack.saveOrUpdateData(value: boolean, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    //Retrieving String//
    static func boolean(forKey: String) -> Bool?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnBool =  returnData as? Bool
            return returnBool
        }
        return nil
    }

    
    
    //Storing Array//
    static func set(array: Array<Any>, forKey:String)
    {
        let arrayAsString: String = array.description
        DataModelStack.saveOrUpdateData(value: arrayAsString, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    
    //Retrieving Array//
    static func array(forKey: String) -> Array<Any>?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnString =  returnData as? String ?? ""
            if let data = returnString.data(using: String.Encoding.utf8)
            {
                if let arrayOfAny = try? JSONSerialization.jsonObject(with: data, options: []) as? [Any] {
                    return arrayOfAny
                }
            }
        }
        return nil
    }
    
    
    //Storing Dictionary//
    static func set(dictionary: Dictionary<String, Any>, forKey:String)
    {
        let thisJSON = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        let datastring:String = String(data: thisJSON!, encoding: String.Encoding.utf8)!
        
        DataModelStack.saveOrUpdateData(value: datastring, forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
    }
    
    
    //Retrieving Dictionary//
    static func dictionary(forKey: String) -> Dictionary<String, Any>?
    {
        if let returnData = DataModelStack.fetchData(forKey: forKey, schemaAttribute: FSSConstants.SchemaAttribute.anyObject)
        {
            let returnString =  returnData as? String ?? ""
            if let data = returnString.data(using: String.Encoding.utf8)
            {
                if let dictionaryOfAny = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any> {
                    return dictionaryOfAny
                }
            }
        }
        return nil
    }
    
    
    //Retrieving Custom Classs Object//
    static func set<T:Encodable>(customObject object: T, inKey key: String)
    {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object)
        {
            FastSecureStorage.set(string: encoded.toString() ?? "", forKey: key)
        }
    }
    
    
    //  Storing Custom Classs Object//
    static func customObject<T:Decodable>(object type:T.Type, fromKey key: String) -> T?
    {
        if let strClassObject = FastSecureStorage.string(forKey: key),
            let data = strClassObject.toData()
        {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(type, from: data)
            {
                return object
            }
            else
            {
                print("Couldnt decode object")
                return nil
            }
        }
        else
        {
            print("Couldnt find key")
            return nil
        }
    }
}


