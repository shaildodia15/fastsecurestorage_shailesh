//
//  FSSConstants.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shailesh Dodia on 17/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation

class FSSConstants: NSObject
{
    static let managedObjContext = DataModelStack.persistentContainer.viewContext

    struct FSSDbValue {
        static let persistentContainer = "FastSecureStorage"
        static let entityName = "DataStorage"
    }
    
    struct SchemaAttribute
    {
        static let key = "key"
        static let string = "string"
        static let decimal = "decimal"
        static let int = "int"
        static let float = "float"
        static let double = "double"
        static let boolean = "boolean"
        static let anyObject = "anyObject"
    }
}
