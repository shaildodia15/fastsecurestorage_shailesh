//
//  DataModelStack.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shailesh Dodia on 17/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import UIKit
import CoreData

class DataModelStack: NSObject
{
    static var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: FSSConstants.FSSDbValue.persistentContainer)
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        return container
    }()
    
    
    static func saveContext ()
    {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            }
            catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    fileprivate static func saveContext(objTemporaryID: NSManagedObjectID)
    {
        do {
            try FSSConstants.managedObjContext.save()
            if !objTemporaryID.isTemporaryID
            {
                //print("Context Saved..")
            }
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    
    static func saveOrUpdateData(value: Any, forKey: String, schemaAttribute: String)
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: FSSConstants.FSSDbValue.entityName)
        let predicate = NSPredicate(format: "\(FSSConstants.SchemaAttribute.key) = %@", forKey)
        fetchRequest.predicate = predicate
        
        let valueToStore = ["fssKey":value]
        let thisJSON = try? JSONSerialization.data(withJSONObject: valueToStore, options: .prettyPrinted)
        let datastring:String = String(data: thisJSON!, encoding: String.Encoding.utf8)!
        let encryptedString = datastring.encryptString()

        if let result = try? FSSConstants.managedObjContext.fetch(fetchRequest), result.count > 0
        {
            for entityToUpdate in (result as! [NSManagedObject])
            {
                entityToUpdate.setValue(encryptedString, forKey: schemaAttribute)
                saveContext(objTemporaryID: entityToUpdate.objectID)
                break
            }
        }
        else
        {
            let entity = NSEntityDescription.entity(forEntityName: FSSConstants.FSSDbValue.entityName, in: FSSConstants.managedObjContext)
            let newData = NSManagedObject(entity: entity!, insertInto: FSSConstants.managedObjContext)
            newData.setValue(forKey, forKey: FSSConstants.SchemaAttribute.key)
            newData.setValue(encryptedString, forKey: schemaAttribute)
            
            saveContext(objTemporaryID: newData.objectID)
        }
    }
    
    
    static func fetchData(forKey: String, schemaAttribute: String) -> Any?
    {
        var returnValue:Any? = nil
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: FSSConstants.FSSDbValue.entityName)
        let predicate = NSPredicate(format: "\(FSSConstants.SchemaAttribute.key) = %@", forKey)
        fetchRequest.predicate = predicate

        do {
            let result = try FSSConstants.managedObjContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject]
            {
                let dataString = data.value(forKey: schemaAttribute) as? String
                let decryptedString = dataString?.decryptString()
                if let returnData = decryptedString!.data(using: String.Encoding.utf8)
                {
                    if let dictionaryOfAny = try? JSONSerialization.jsonObject(with: returnData, options: []) as? Dictionary<String, Any>
                    {
                        returnValue = dictionaryOfAny?["fssKey"]
                    }
                }
                break
            }
        } catch {
            print("Failed")
        }
        
        return returnValue
    }
}
