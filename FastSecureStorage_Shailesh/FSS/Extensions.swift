//
//  Extensions.swift
//  FastSecureStorage_Shailesh
//
//  Created by Shaileshkumar Dodhia on 19/02/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation
import CommonCrypto

extension Data
{
    func toString() -> String?
    {
        return String(data: self, encoding: .utf8)
    }
}

extension String
{
    func toData() -> Data?
    {
        return Data(self.utf8)
    }
    
    func encryptString() -> String?
    {
        let data = self.data(using: String.Encoding.utf8)
        let encryptedData = CryptoUtility.Crypt(data: data!, operation: kCCEncrypt)!
        let base64Str = encryptedData.base64EncodedString()
        return base64Str
    }
    
    func decryptString() -> String?
    {
        let str1Data = Data(base64Encoded: self)
        let decrypt = CryptoUtility.Crypt(data: str1Data!, operation: kCCDecrypt)
        let plain = String(data: decrypt!, encoding: .utf8)
        return plain
    }
}

